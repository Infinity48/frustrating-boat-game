extends Panel

func _process(delta):
	if Input.is_action_just_released('ui_cancel'):
		if visible:
			if OS.get_name() != 'HTML5': get_tree().notification(MainLoop.NOTIFICATION_WM_QUIT_REQUEST)
		else:
			visible = true
			get_tree().paused = true
	elif Input.is_action_just_released('ui_accept'):
		visible = false
		get_tree().paused = false
