extends StaticBody2D

const SPEED = 320

func _physics_process(delta):
	position.y -= SPEED * delta
	if position.y <= -32:
		position.y = 0
