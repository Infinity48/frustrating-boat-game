extends Node

var _file = File.new()
var scores:Array = [
	{
		'name':'Dev', 
		'score':80
	}
]
var current:int = 0

func _ready():
	if _file.file_exists('user://highscores.json'):
		_file.open('user://highscores.json', File.READ)
		scores = parse_json(_file.get_as_text())
		_file.close()

func save(name:String):
	var newentry = {'name':name, 'score':current}
	for i in range(len(scores)):
		var entry = scores[i]
		if current > entry['score']:
			scores.insert(i, newentry)
			break
		elif i == len(scores) - 1:
			scores.append(newentry)
	_file.open('user://highscores.json', File.WRITE)
	_file.store_string(to_json(scores))
	_file.close()
