extends Panel

func _process(delta):
	if Input.is_action_just_released('ui_accept') and visible:
		get_tree().paused = false
		get_tree().reload_current_scene()

func init_items():
	$List.clear()
	for i in range(len(Score.scores)):
		var entry = Score.scores[i]
		$List.add_item(str(i+1), null, false)
		$List.add_item(entry['name'], null, false)
		$List.add_item(str(entry['score']) + 'm', null, false)

func _on_Player_died():
	visible = true
	get_tree().paused = true
	init_items()

func _on_OKButton_pressed():
	Score.save($NameEntry.text)
	$OKButton.disabled = true
	init_items()
