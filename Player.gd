extends KinematicBody2D

var velocity = Vector2()
var target = Vector2()
var health = 3
var score = 0.0
onready var _rocks = get_parent().get_node('Rocks')
signal died

func _input(event):
	if event is InputEventMouseMotion:
		velocity.x = event.position.x - position.x

func _process(delta):
	modulate.g = (health - 1) * 0.5
	modulate.b = (health - 1) * 0.5
	score += 0.03125
	Score.current = int(score)
	$'../GUI/Score'.text = 'Distance: ' + str(Score.current) + 'm'
	if health <= 0:
		emit_signal('died')

func _physics_process(delta):
	velocity.x = move_and_slide(velocity).x
