extends Area2D

const SPEED = 320
var _scene = load('res://Rock.tscn')
var repositioned = false

func _ready():
	randomize()
	position.x = rand_range(200, 792)

func _physics_process(delta):
	position.y -= SPEED * delta
	if position.y <= -40:
		var newrock = _scene.instance()
		var i = randi() % 3
		newrock.position.y = 700 + i * 80
		get_parent().add_child(newrock)
		queue_free()

func _on_body_entered(body):
	if body is KinematicBody2D:
		body.health -= 1
		body.get_node('Hit').play()
