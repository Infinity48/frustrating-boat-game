extends Label

func _ready():
	get_tree().paused = true

func _input(event):
	if !event is InputEventMouseMotion:
		get_tree().paused = false
		queue_free()
